<?php

/**
 * Implements hook_field_info().
 */
function disqus_field_info() {
  $info['disqus'] = array(
    'label' => t('Disqus'),
    'description' => t('This field stores varchar text in the database.'),
    'default_widget' => 'disqus_default',
    'default_formatter' => 'disqus_embed',
  );

  return $info;
}

/**
 * Implements hook_field_widget_info().
 */
function disqus_field_widget_info() {
  $info['disqus_default'] = array(
    'label' => t('Comment settings'),
    'field types' => array('disqus'),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_CUSTOM,
    ),
  );

  return $info;
}

/**
 * Implements hook_field_is_empty().
 */
function disqus_field_is_empty($item, $field) {
  return !isset($item['status']);
}

/**
 * Implements hook_field_access().
 */
function disqus_field_access($op, $field, $entity_type, $entity, $account) {
  if ($field['type'] == 'disqus' && !variable_get('disqus_domain', '')) {
    return FALSE;
  }
}

/**
 * Implements hook_field_widget_form().
 */
function disqus_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  if ($delta > 0) {
    return $element;
  }

  // Default is always enabled.
  $item = isset($items[$delta]) ? $items[$delta] : array('status' => 1);

  $element += array(
    '#type' => !empty($element['#entity']) ? 'fieldset' : 'container',
    '#title' => t('Disqus'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('disqus-settings-form'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'disqus') . '/disqus.form.js'),
    ),
    '#weight' => 30,
    '#tree' => FALSE,
  );
  $element['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Comments enabled'),
    '#default_value' => $item['status'],
    '#attributes' => array(
      'class' => array('disqus-status'),
    ),
    '#parents' => array_merge($element['#field_parents'], array($element['#field_name'], $element['#language'], $delta, 'status')),
  );
  return $element;
}

/**
 * Implements hook_field_formatter_info().
 */
function disqus_field_formatter_info() {
  $info['disqus_embed'] = array(
    'label' => t('Embedded comments'),
    'field types' => array('disqus'),
  );
  $info['disqus_commentrss'] = array(
    'label' => t('commentRss'),
    'field types' => array('disqus'),
  );
  /*$info['disqus_link'] = array(
    'label' => t('Comment count (rendered in entity link)'),
    'field types' => array('disqus'),
  );*/

  return $info;
}

/**
 * Implements hook_field_prepare_view().
 */
function disqus_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  foreach ($entities as $id => $entity) {
    if (empty($items[$id]) && !empty($instances[$id]['default_value'])) {
      $items[$id] = $instances[$id]['default_value'];
    }
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function disqus_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'disqus_embed':
      $element[0] = array(
        '#theme' => 'disqus_embed',
        '#entity_type' => $entity_type,
        '#entity' => $entity,
      );
      break;

    case 'disqus_commentrss':
      $entity->rss_namespaces['xmlns:wfw'] = 'http://wellformedweb.org/CommentAPI/';
      $entity->rss_elements[] = array(
        'key' => 'wfw:commentRss',
        'value' => _disqus_get_comment_rss_url($entity_type, $entity),
      );
      break;

    /*case 'disqus_link':
      $uri = entity_uri($entity_type, $entity);
      if (empty($uri['path']) || !field_access('view', $field, $entity_type, $entity)) {
        break;
      }

      $variables = disqus_entity_variables($entity_type, $entity);
      $links = array();
      $links['comment-count'] = array(
        'title' => 'Comments',
        'href' => $uri['path'],
        'fragment' => 'disqus_thread',
        'attributes' => array(
          'data-disqus-identifier' => $variables['disqus_identifier'],
        ),
      ) + $uri['options'];
      $entity->content['links']['disqus'] = array(
        '#theme' => 'links__' . $entity_type . '__disqus',
        '#links' => $links,
        '#attributes' => array('class' => array('links', 'inline')),
        '#attached' => array(
          'library' => array(
            array('disqus', 'count'),
          ),
        ),
        'disqus_add_variable' => array(
          //array('disqus_shortname', $shortname),
        ),
      );
      break;*/

  }

  return $element;
}
