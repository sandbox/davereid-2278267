<div id="disqus_thread"<?php print $attributes; ?>><?php print render($content); ?></div>
<noscript><?php print t('Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a>'); ?></noscript>
