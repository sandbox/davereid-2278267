
(function ($) {

Drupal.behaviors.disqusFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.disqus-settings-form', context).drupalSetSummary(function (context) {
      if ($('input.disqus-status:checked', context).length) {
        return Drupal.checkPlain($('input.disqus-status:checked', context).next('label').text()) + ': ' + Drupal.t('Yes');
      }
      else {
        return Drupal.checkPlain($('input.disqus-status', context).next('label').text()) + ': ' + Drupal.t('No');
      }
    });
  }
};

})(jQuery);
