(function ($) {

/**
 * Drupal Disqus behavior.
 */
Drupal.behaviors.disqus = {
  attach: function (context, settings) {
    $('#disqus_thread.disqus-lazy-click', context).once('disqus-lazy-click', function() {
      $(this).css('cursor', 'pointer');
      $(this).bind('click', function (event) {
        $.ajax({
          type: 'GET',
          url: '//' + disqus_shortname + '.disqus.com/embed.js',
          dataType: 'script',
          cache: false
        });
      });
    });

    $('body', context).once('disqus-lazy-scroll', function() {
      $(window).scroll(function scrollHandler(event) {
        if ($(window).scrollTop() + $(window).height() > $('#disqus_thread.disqus-lazy-scroll').offset().top) {
          $.ajax({
            type: 'GET',
            url: '//' + disqus_shortname + '.disqus.com/embed.js',
            dataType: 'script',
            cache: false
          });
          $(window).unbind('scroll', scrollHandler);
        }
      });
    });
  }
}

})(jQuery);
