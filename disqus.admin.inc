<?php

function disqus_settings_form($form, $form_state) {
  $form['disqus_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Shortname'),
    '#description' => t('The website shortname that you registered Disqus with. If you registered http://example.disqus.com, you would enter "example" here.'),
    '#default_value' => variable_get('disqus_domain', ''),
  );
  $form['disqus_lazy_load'] = array(
    '#type' => 'radios',
    '#title' => t('Lazy-load settings'),
    '#options' => array(
      DISQUS_LAZY_LOAD_DISABLED => t('Disabled'),
      DISQUS_LAZY_LOAD_ON_SCROLL => t('Load when scrolled into viewport'),
      DISQUS_LAZY_LOAD_ON_CLICK => t('Load on demand (clicked)'),
    ),
    '#default_value' => variable_get('disqus_lazy_load', DISQUS_LAZY_LOAD_DISABLED),
  );
  $form['disqus_lazy_load_text'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to display for user to click'),
    '#description' => t('Use <code>{{ disqus-comment-count }}</code> to insert the comment count.'),
    '#default_value' => variable_get('disqus_lazy_load_text', '<p>Click to view comments.</p>'),
    '#states' => array(
      'visible' => array(
        'input[name="disqus_lazy_load"]' => array('value' => DISQUS_LAZY_LOAD_ON_CLICK),
      ),
    ),
  );

  return system_settings_form($form);
}
