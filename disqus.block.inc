<?php

/**
 * Implements hook_block_info().
 */
function disqus_block_info() {
  $info['embed'] = array(
    'info' => t('Disqus Comments'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );

  return $info;
}

/**
 * Implements hook_block_view().
 */
function disqus_block_view($delta) {
  $block = array();

  switch ($delta) {
    case 'embed':
      $block['subject'] = t('Comments');
      $block['content'] = array('#theme' => 'disqus_embed');
      break;

  }

  return $block;
}
